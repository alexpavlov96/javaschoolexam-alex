package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    
	int calculatePyramidSize( int arraySize ) {
		int res = 1, add = 2;
	      if (arraySize < 3)
	    	return 0;
	      while (res < arraySize) {
	    	res += add;
	    	add++;
	      }
	      if (res != arraySize) {
	    	  return -1;
	      }
	      else {
	    	  return add - 3;
	      }
	}
	
	private boolean hasNull( List<Integer> inputNumbers ) {
	  for (int i = 0; i < inputNumbers.size(); i++) {
		  if (inputNumbers.get(i) == null) {
			  return true;
		  }
	  }
	  return false;
	}
	
	public int[][] buildPyramid(List<Integer> inputNumbers) {
        int arraySize = inputNumbers.size();
		int pyramidSize = calculatePyramidSize(arraySize);
		if (pyramidSize == -1)
		  throw new CannotBuildPyramidException();
  	    if (hasNull(inputNumbers))
  	      throw new CannotBuildPyramidException();
		int numColomns = 3 + pyramidSize * 2;
  	    int numRows = pyramidSize + 2;
  	    try {
  	    	int resultArray[][] = new int[numRows][numColomns];
	  	    int currentIndex = 0;
	  	    inputNumbers.sort(null);
	  	    boolean isZero = false;
	  	    for (int i = 0; i < numRows; i++) {
	  	    	isZero = false;
	  	    	for (int j = 0; j < numRows - 1 - i; j++)
	  	    		resultArray[i][j] = 0;
	  	    	for (int j = numRows - 1 - i; j < numRows + i; j++) {
	  	    		if (isZero) {
	  	    			resultArray[i][j] = 0;	
	  	    		}
	  	    		else {
	  	    			resultArray[i][j] = inputNumbers.get(currentIndex++);
	  	    		}
	  	    		isZero = !isZero;
	  	    	}
	  	    	for (int j = numRows + i; j < numColomns; j++)
	  	    		resultArray[i][j] = 0;
	  	    }
	        return resultArray;
  	      } catch(OutOfMemoryError e) {
	    	  throw new CannotBuildPyramidException();
	      } catch(NegativeArraySizeException e1) {
	    	  throw new CannotBuildPyramidException();
	      }
    }
}
