package com.tsystems.javaschool.tasks.subsequence;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
    	if (x.size() == 0 && y != null)
    	  return true;
    	if (y.size() < x.size())
          return false;
        int iX = 0, iY = 0;
        while (iY < y.size() && iX < x.size()) {
          while (x.get(iX) != y.get(iY)) {
        	iY++;
        	if (iY >= y.size())
        	  return false;
          }
          if (x.get(iX) == y.get(iY)) {
        	iX++;
        	iY++;
        	if (iX == x.size())
        	  return true;
        	if (iY >= y.size())
          	  return false;
          }
        }
        return false;
    }
}
